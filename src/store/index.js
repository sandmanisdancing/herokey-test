import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: {
      name: 'Alexander'
    },
    show: {
      showsList: [
        {
          id: 1,
          title: 'Melt Festival',
          icon: 'melt-festival',
          iconColor: '#FFF',
          bgColor: '#F298C0'
        },
        {
          id: 2,
          title: 'splash! Festival',
          icon: 'splash-festival',
          iconColor: '#D12233',
          bgColor: '#EDF2EE'
        }
      ],
      isMenuShown: false
    }
  },
  getters: {},
  mutations: {
    showGoodliveMenu (state) {
      state.show.isMenuShown = true
    },
    hideGoodliveMenu (state) {
      state.show.isMenuShown = false
    }
  },
  actions: {}
})