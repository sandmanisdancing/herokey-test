import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

// using functions to lazy load routes
const Index = () => import('@/components/pages/Index')
const ShowDefault = () => import('@/components/pages/ShowDefault')
const Show = () => import('@/components/pages/Show')

export default new Router({
  mode: 'history',

  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  },

  routes: [
    {
      path: '/',
      name: 'index',
      component: Index
    },
    {
      path: '/show',
      name: 'show-default',
      component: ShowDefault
    },
    {
      path: '/show/:showId',
      name: 'show',
      component: Show
    }
  ]
})
